import { createRouter, createWebHistory } from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Help from './views/Help.vue'

import ValeGuardian from './views/ValeGuardian.vue'
import SpiritWoods from './views/SpiritWoods.vue'
import Gorseval from  './views/Gorseval.vue'
import Sabetha from  './views/Sabetha.vue'

import Slothasor from './views/Slothasor.vue'
import PrisonCamp from './views/PrisonCamp.vue'
import Matthias from './views/Matthias.vue'

import Escort from './views/Escort.vue'
import KeepConstruct from './views/KeepConstruct.vue'
import TwistedCastle from './views/TwistedCastle.vue'
import Xera from './views/Xera.vue'

import Cairn from './views/Cairn.vue'
import Mursaat from './views/MursaatOverseer.vue'
import Samarog from './views/Samarog.vue'
import Deimos from './views/Deimos.vue'

import Desmina from './views/Desmina.vue'
import River from './views/River.vue'
import Statues from './views/Statues.vue'
import Dhuum from './views/Dhuum.vue'

import ConjuredAmalgamate from './views/ConjuredAmalgamate.vue'
import Appraisal from './views/Appraisal.vue'
import Largos from './views/Largos.vue'
import Qadim from './views/Qadim.vue'

import Gates from './views/Gates.vue'
import Sabir from './views/Sabir.vue'
import Adina from './views/Adina.vue'
import Peerless from './views/Peerless.vue'

const routes = [
	{ path: '/', name: 'home', props: true, component: Home },
	{ path: '/about', name: 'about', component: About },
	{ path: '/help', name: 'help', component: Help },

	{ path: '/vg', name: 'vg', props: true, component: ValeGuardian },
	{ path: '/spirit', name: 'spirit', props:true, component: SpiritWoods },
	{ path: '/gorseval', name: 'gorseval', props: true, component: Gorseval },
	{ path: '/sabetha', name: 'sabetha', props: true, component: Sabetha },

	{ path: '/slothasor', name: 'slothasor', props: true, component: Slothasor },
	{ path: '/trio', name: 'trio', props: true, component: PrisonCamp },
	{ path: '/matthias', name: 'matthias', props: true, component: Matthias },

	{ path: '/escort', name: 'escort', props: true, component: Escort },
	{ path: '/kc', name: 'kc', props: true, component: KeepConstruct },
	{ path: '/tc', name: 'tc', props: true, component: TwistedCastle },
	{ path: '/xera', name: 'xera', props: true, component: Xera },

	{ path: '/cairn', name: 'cairn', props: true, component: Cairn },
	{ path: '/mo', name: 'mo', props: true, component: Mursaat },
	{ path: '/samarog', name: 'samarog', props: true, component: Samarog },
	{ path: '/deimos', name: 'deimos', props: true, component: Deimos },

	{ path: '/desmina', name: 'desmina', props: true, component: Desmina },
	{ path: '/river', name: 'river', props: true, component: River },
	{ path: '/statues', name: 'statues', props: true, component: Statues },
	{ path: '/dhuum', name: 'dhuum', props: true, component: Dhuum },

	{ path: '/amalgamate', name: 'amalgamate', props: true, component: ConjuredAmalgamate },
	{ path: '/appraisal', name: 'appraisal', props: true, component: Appraisal },
	{ path: '/largos', name: 'largos', props: true, component: Largos },
	{ path: '/qadim', name: 'qadim', props: true, component: Qadim },

	{ path: '/gates', name: 'gates', props: true, component: Gates },
	{ path: '/sabir', name: 'sabir', props: true, component: Sabir },
	{ path: '/adina', name: 'adina', props: true, component: Adina },
	{ path: '/peerless', name: 'peerless', props: true, component: Peerless },
]
const router = createRouter({
	history: createWebHistory(),
	routes
})

export default router