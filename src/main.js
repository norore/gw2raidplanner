import { createApp } from 'vue'
import App from './App.vue'
import router from './router.js'
import '~bootstrap/js/src/collapse.js'

import './assets/style.scss'

const app = createApp(App)
app.use(router)
app.mount('#app')
